import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';
import Component from './components/Component';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

//const pizzaList = new PizzaList([]);
Router.routes = [{ path: '/', page: pizzaList, title: 'La carte' }];

const mess = "<small>les pizza c'est la vie</small>";
document.querySelector('.logo').innerHTML += mess;

Router.menuElement = document.querySelector('.mainMenu');

const tmp =
	document
		.querySelector('header')
		.querySelector('a.pizzaListLink')
		.getAttribute('class') + ' active';
document
	.querySelector('header')
	.querySelector('a.pizzaListLink')
	.setAttribute('class', tmp);

document.querySelector('.newsContainer').setAttribute('style', '');

function handleClick(event) {
	document
		.querySelector('.newsContainer')
		.setAttribute('style', 'display:none');
	console.log(event);
}
const link = document.querySelector('.closeButton');
link.addEventListener('click', handleClick);

const pizzaList = new PizzaList(data),
	aboutPage = new Component('section', null, 'Ce site est génial'),
	pizzaForm = new Component(
		'section',
		null,
		'Ici vous pourrez ajouter une pizza'
	);

Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];

Router.menuElement = document.querySelector('.mainMenu');

Router.navigate('/'); // affiche une page vide
pizzaList.pizzas = data; // appel du setter
Router.navigate('/'); // affiche la liste des pizzas
